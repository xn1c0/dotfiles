#!/usr/bin/env bash

killall emacs
killall waybar
killall dunst

waybar &
dunst &
#swww init &
#swww img "$HOME/.wallpapers/wp01.gif"
signal-desktop --use-tray-icon --start-in-tray &
bitwarden-desktop --enable-features=UseOzonePlatform,WaylandWindowDecorations --ozone-platform-hint=wayland &
hyprctl dispatch closewindow address:"$(hyprctl -j clients | jq -j '.[] | select(.class=="Bitwarden") | .address')" &
#hyprctl dispatch closewindow address:"$(hyprctl -j clients | jq -j '.[] | select(.class=="signal") | .address')" &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/bin/emacs --fg-daemon &

gsettings set org.gnome.desktop.interface icon-theme 'Tela-circle-dracula'
gsettings set org.gnome.desktop.interface gtk-theme 'Catppuccin-Mocha-Standard-Rosewater-Dark'
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
gsettings set org.gnome.desktop.interface font-name 'JetBrainsMono'

sleep 1
killall -e xdg-desktop-portal-hyprland
killall -e xdg-desktop-portal-wlr
killall xdg-desktop-portal
/usr/lib/xdg-desktop-portal-hyprland &
sleep 2
/usr/lib/xdg-desktop-portal &

systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=Hyprland
